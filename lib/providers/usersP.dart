import 'dart:convert';

import 'package:get/get.dart';

class UserProvider extends GetConnect {
  final url =
      "https://getconnect-95f7c-default-rtdb.asia-southeast1.firebasedatabase.app/";

  // Get request
  Future<Response> getUser(int id) => get(url);

  // Simpan user ke database
  Future<Response> postData(String name, String email, String phone) {
    final body = jsonEncode({
      "name": name,
      "email": email,
      "phone": phone,
    });

    return post(url + "users.json", body);
  }

  // Hapus data
  Future<Response> deleteData(String id) {
    return delete(url + "users/$id.json");
  }

  // edit data
  Future<Response> editData(
      String id, String name, String email, String phone) {
    final body = json.encode({
      "name": name,
      "email": email,
      "phone": phone,
    });
    return patch(url + "users/$id.json", body);
  }
}
